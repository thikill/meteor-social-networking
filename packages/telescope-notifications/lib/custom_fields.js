Settings.addField({
  fieldName: 'emailNotifications',
  fieldSchema: {
    type: Boolean,
    optional: true,
    defaultValue: true,
    autoform: {
      group: 'notifications',
      instructions: 'Enable Thông báo qua Email for new posts and new comments (requires restart).'
    }
  }
});

// make it possible to disable notifications on a per-comment basis
Comments.addField(
  {
    fieldName: 'disableNotifications',
    fieldSchema: {
      type: Boolean,
      optional: true,
      autoform: {
        omit: true
      }
    }
  }
);

// Add notifications options to user profile settings
Users.addField([
  {
    fieldName: 'telescope.notifications.users',
    fieldSchema: {
      label: 'Thành viên mới',
      type: Boolean,
      optional: true,
      defaultValue: false,
      editableBy: ['admin'],
      autoform: {
        group: 'Thông báo qua Email'
      }
    }
  },
  {
    fieldName: 'telescope.notifications.posts',
    fieldSchema: {
      label: 'Video mới',
      type: Boolean,
      optional: true,
      defaultValue: false,
      editableBy: ['admin', 'member'],
      autoform: {
        group: 'Thông báo qua Email'
      }
    }
  },
  {
    fieldName: 'telescope.notifications.comments',
    fieldSchema: {
      label: 'Bình luận mới trên video của bạn',
      type: Boolean,
      optional: true,
      defaultValue: true,
      editableBy: ['admin', 'member'],
      autoform: {
        group: 'Thông báo qua Email'
      }
    }
  },
  {
    fieldName: 'telescope.notifications.replies',
    fieldSchema: {
      label: 'Phản hồi mới trên bình luận của bạn',
      type: Boolean,
      optional: true,
      defaultValue: true,
      editableBy: ['admin', 'member'],
      autoform: {
        group: 'Thông báo qua Email'
      }
    }
  }
]);
